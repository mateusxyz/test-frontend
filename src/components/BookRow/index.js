import React, { useState, useEffect,useRef } from 'react';
import {Button,TextField} from '@material-ui/core';
export default (props)=>{
    const refMenu = useRef(null);
    const [edit,setEdit]=useState(false)
    const [nome,setNome]=useState("")
    const [autor,setAutor]=useState("")
    const [preco,setPreco]=useState("")
    const [id,setId]=useState("")
    const [errN,setErrN]=useState(false)
    const [errA,setErrA]=useState(false)
    const [errP,setErrP]=useState(false)
        useEffect(()=>{
            setNome(props.nome)
            setAutor(props.autor)
            setPreco(props.preco)
            setId(props.id)
        },[])
        function dropDownHandler() {
            refMenu.current.classList.toggle("options-menu")
            refMenu.current.classList.toggle("options-closed")
        }
        function editMode() {
            setEdit(!edit)
        }
        function saveEdit() {
            props.onEdit({nome,autor,id,preco})
            editMode()
        }
        function cancelEdit() {
            setNome(props.nome)
            setAutor(props.autor)
            setPreco(props.preco)
            editMode()
        }
        //Regex tester para o preço
        const rex = /^[0-9,+]+$/;
        function precoTest(val) {
            if (rex.test(val)||!val) {
                setPreco(val)
            }else{
                setPreco(preco)
            }
            
            
        }
        //Função para renderização condicional da msg de erro
        function tester() {
            if (autor&&nome&&preco) {
                saveEdit()
            } else {
                if(!autor){
                    setErrA(true)
                }
                if(!nome){
                    setErrN(true)
                }
                if(!preco){
                    setErrP(true)
                }
            }
            
        } 
        //renderização condicional do modo de edição
        if(edit){
            return(
            
                <tr>
                    {/*Condição ternaria para a renderização condicional da propriedade erro */}
                    <td>{errN?<TextField multiline error defaultValue={nome} onChange={(val)=>{setNome(val.target.value)}}/>:<TextField multiline defaultValue={nome} onChange={(val)=>{setNome(val.target.value)}}/>}</td>
                    <td>{errA?<TextField multiline error defaultValue={autor} onChange={(val)=>{setAutor(val.target.value)}}/>:<TextField multiline defaultValue={autor} onChange={(val)=>{setAutor(val.target.value)}}/>}</td>
                    <td>{errP?<TextField multiline error value={preco} onChange={(val)=>precoTest(val.target.value)}/>:<TextField multiline value={preco} onChange={(val)=>precoTest(val.target.value)}/>}</td>
                    <td><div className="options" onClick={()=>{dropDownHandler()}}>
                        ...
                        <div className="options-closed" ref={refMenu}>
                            
                            <Button className="button" variant="contained" color="primary" onClick={()=>{tester()}}> Salvar </Button>
                            <Button className="button" variant="contained" color="secondary" onClick={()=>{cancelEdit()}}>Cancelar</Button>
                        </div>
                    </div></td>
                </tr>
        
                )
        }else{
            return(
            
                <tr>
                    <td>{nome}</td>
                    <td>{autor}</td>
                    <td>R$ {preco}</td>
                    <td><div className="options" onClick={()=>{dropDownHandler()}}>
                        ...
                        <div className="options-closed" ref={refMenu}>
                            <Button className="button" variant="contained" color="primary" onClick={()=>{editMode()}}> Editar </Button>
                            <Button className="button" variant="contained" color="secondary" onClick={()=>{props.onDelete(id)}}>Excluir</Button>
                        </div>
                    </div></td>
                </tr>
        
                )
        }
    
}
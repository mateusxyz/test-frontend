import './index.css';
import {
  Link
} from "react-router-dom";
function App() {
  return (
    <div>
        <header className="header">
          <Link to="/">
            <div className="title">
              <img src="/logo.png"></img>
              <h2>Libraro</h2>
            </div>
          </Link>
          <div className="nav">
            <Link to="/">Lista de Livros</Link>
            <Link to="/create">Cadastrar Livro</Link>
            <a href="">Sobre</a>
          </div>
        </header>
        
    </div>
    
  );
}

export default App;

import logo from './logo.svg';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import ListBook from './screens/ListBooks'
import Create from './screens/Create-Edit'

function App() {
  return (
    <Router>
        <div className="App">
          <Switch>
          <Route path="/about">
            <div></div>
          </Route>
          <Route path="/create">
            <Create />
          </Route>
          <Route path="/">
            <ListBook />
          </Route>
        </Switch>
        </div>
        
    </Router>
    
  );
}

export default App;

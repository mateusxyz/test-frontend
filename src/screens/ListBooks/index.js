import React, { useState, useEffect } from 'react';
import './index.css';
import Header from '../../components/Header';
import BookRow from '../../components/BookRow'
import {api,axios} from '../../components/Api'

function App() {
    
    
    const [lista,setLista]=useState()
    //Funções da API
    function onDelete(id) {
        axios.delete(`${api}${id}/`)
        .then((res)=>{
            console.log(res)
            setLista(lista.filter((val)=>val.id!=id))
        })
        .catch((err)=>{
            console.error(err)
        })
    }
    function onEdit(par) {
        axios.put(`${api}${par.id}/`,par)
        .then((res)=>{
            console.log(res)
        })
        .catch((err)=>{
            console.error(err)
        })
    }
    //Requisita as informações da API assim que o componente monta
    useEffect(()=>{
        axios.get(`${api}`)
        .then((res)=>{
            setLista(res.data)
        })
        .catch((err)=>{
            console.error(err)
        })

    }, [])

  return (
    <div><Header />
    <div className="tableContainer">
    
            <table>
              <tbody>
                {lista?<tr>
                    <th>Nome</th>
                    <th>Autor</th>
                    <th>Preço</th>
                    <th>Opções</th>
                </tr>:undefined}
                {lista?lista.map((val)=>{
                    return(
                    <BookRow key={Math.random()} nome={val.nome} autor={val.autor} preco={val.preco} id={val.id} onEdit={onEdit} onDelete={onDelete}></BookRow>
                    ) 
                }):undefined}
                
                
                
                </tbody>  
            </table>

    </div>
    </div>
    
  );
}

export default App;
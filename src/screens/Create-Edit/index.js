import Header from '../../components/Header';
import "./index.css"
import {TextField, Button} from '@material-ui/core'
import { useState } from 'react';
import {api,axios} from '../../components/Api'
export default ()=>{
    //Função para renderização condicional da msg de erro
    function tester() {
        if (autor&&nome) {
            return true
        } else {
            setErr(true)
            return false
        }
        
    }    
    //Função para inserir registro no db
    function create() {

        if (tester()) {
            axios.post(api,{nome,autor,preco})
            .then((res)=>{
                console.log(res)
                setNome("")
                setPreco("")
                setAutor("")
            }).catch((res)=>{
                console.log(res)
            })
        }
        
    }
    //Regex tester para o preço
    const rex = /^[0-9,+]+$/;
    function precoTest(val) {
        if (rex.test(val)||!val) {
            setPreco(val)
        }else{
            setPreco(preco)
        }
        
        
    }
    
    const [nome,setNome]=useState("")
    const [autor,setAutor]=useState("")
    const [preco,setPreco]=useState("")
    const [err,setErr]=useState(false)
    return (
        <div>
            <Header />
            <div className="createContainer">
                Cadastro de Livros
                {err?<span className="error">Preencha todos os campos com *</span>:undefined}
                <div className="multipleText">
                    <TextField className="fieldText" value={nome} onChange={(val)=>{setNome(val.target.value)}}  label="Nome*" />
                    <TextField className="fieldText" value={autor} onChange={(val)=>{setAutor(val.target.value)}} label="Autor*" />
                </div>
                <div className="multipleText">
                    <TextField className="fieldText" value={preco} onChange={(val)=>precoTest(val.target.value)} label="Preço*" />
                    <div className="buttonContainer">
                        <Button onClick={create} className="sbutton" variant="contained" color="primary">
                                Salvar
                        
                        </Button>
                    </div>
                        
                </div>
            </div>
            
        </div>
    )
}